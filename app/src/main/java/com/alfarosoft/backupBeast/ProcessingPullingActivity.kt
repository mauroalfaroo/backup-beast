package com.alfarosoft.backupBeast

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import android.widget.ToggleButton
import com.alfarosoft.backupBeast.model.constants.ModuleNamesConstants
import com.alfarosoft.backupBeast.services.*

class ProcessingPullingActivity : AppCompatActivity() {
    lateinit var callLogPullingService: CallLogPullingService
    lateinit var smsPullingService: SMSPullingService
    lateinit var mmsPullingService: MMSPullingService
    lateinit var contactsPullingService: ContactsPullingService
    lateinit var generalInfoPullingService: GeneralInfoPullingService

    lateinit var whatsappButton: ToggleButton
    lateinit var messagesButton: ToggleButton
    lateinit var callsButton: ToggleButton
    lateinit var contactsButton: ToggleButton
    lateinit var photosButton: ToggleButton
    lateinit var generalButton: ToggleButton

    var modulesList: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_processing_pulling)

        initComponents()

        Log.i("Starting data pulling","Backup started");


        if (modulesList.contains(ModuleNamesConstants.MESSAGES_MODULE_NAME)) {
            messagesButton.visibility = View.VISIBLE
            messagesButton.isChecked = true
            Log.i("SMS Messages: ", smsPullingService.listAsJson(smsPullingService.getSMSMessages()))
            Log.i("MMS Messages: ", mmsPullingService.listAsJson(mmsPullingService.getMMSMessages()))

        }

        if (modulesList.contains(ModuleNamesConstants.CONTACTS_MODULE_NAME)){
            contactsButton.visibility = View.VISIBLE
            contactsButton.isChecked = true
            Log.i("Contacts: ", contactsPullingService.listAsJson(contactsPullingService.getContacts()))
        }

        if (modulesList.contains(ModuleNamesConstants.CALLS_MODULE_NAME)) {
            callsButton.visibility = View.VISIBLE
            callsButton.isChecked = true
            Log.i("Calls: ", callLogPullingService.listAsJson(callLogPullingService.getCallLog()))
        }

        if (modulesList.contains(ModuleNamesConstants.GENERAL_INFO_MODULE_NAME)) {
            generalButton.visibility = View.VISIBLE
            generalButton.isChecked = true
            Log.i("General Info: ", generalInfoPullingService.infoAsJson(generalInfoPullingService.getPhoneGeneralInfo()))
        }

        Log.i("Finished","Backup finished");
        Toast.makeText(this, "Your backup has been completed. Check logs for details", Toast.LENGTH_LONG).show()
    }

    private fun initComponents(){
        whatsappButton = findViewById(R.id.whatsappButton2)
        messagesButton = findViewById(R.id.messagesButton2)
        callsButton = findViewById(R.id.callsButton2)
        contactsButton = findViewById(R.id.contactsButton2)
        photosButton = findViewById(R.id.photosButton2)
        generalButton = findViewById(R.id.generalButton2)

        smsPullingService = SMSPullingService(applicationContext.contentResolver)
        mmsPullingService = MMSPullingService(applicationContext.contentResolver)
        contactsPullingService = ContactsPullingService(applicationContext.contentResolver)
        callLogPullingService = CallLogPullingService(applicationContext.contentResolver)
        generalInfoPullingService = GeneralInfoPullingService(packageManager)

        modulesList = intent.getStringArrayListExtra("Modules") as ArrayList<String>;
    }
}