package com.alfarosoft.backupBeast

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.ToggleButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.alfarosoft.backupBeast.model.constants.ModuleNamesConstants
import com.alfarosoft.backupBeast.services.CallLogPullingService
import com.alfarosoft.backupBeast.services.ContactsPullingService
import com.alfarosoft.backupBeast.services.SMSPullingService

class MainActivity : AppCompatActivity() {
    lateinit var whatsappButton: ToggleButton
    lateinit var messagesButton: ToggleButton
    lateinit var callsButton: ToggleButton
    lateinit var contactsButton: ToggleButton
    lateinit var photosButton: ToggleButton
    lateinit var generalButton: ToggleButton
    lateinit var finishTextView: TextView

    val modulesList: MutableList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        askPermissions()
        initComponents()
        initListeners()

    }

    private fun initComponents(){

        whatsappButton = findViewById(R.id.whatsappButton)
        messagesButton = findViewById(R.id.messagesButton)
        callsButton = findViewById(R.id.callsButton)
        contactsButton = findViewById(R.id.contactsButton)
        photosButton = findViewById(R.id.photosButton)
        generalButton = findViewById(R.id.generalButton)

        finishTextView = findViewById(R.id.textViewFinish)
    }

    private fun initListeners() {
        val arrayAdapterItems: ArrayAdapter<String> = ArrayAdapter<String>(this@MainActivity, android.R.layout.simple_list_item_1, modulesList)

        finishTextView.setOnClickListener {
            if(modulesList.size>0){
                val dialog = AlertDialog.Builder(this)
                        .setAdapter(arrayAdapterItems, null)
                        .setTitle(R.string.confirm_dialog_title)
                        .setNegativeButton(R.string.cancel_dialog_button) { view, _ ->
                            view.dismiss()
                        }
                        .setPositiveButton(R.string.confirm_dialog_button) { view, _ ->
                            val intent = Intent(this, ProcessingPullingActivity::class.java)
                            intent.putStringArrayListExtra("Modules", ArrayList(modulesList))
                            startActivity(intent)
                            view.dismiss()
                        }
                        .setCancelable(false)
                        .create()
                dialog.show()
            }
            else {
                val dialog = AlertDialog.Builder(this)
                        .setTitle(R.string.error)
                        .setMessage(R.string.no_module_select_dialog)
                        .setPositiveButton(R.string.ok_dialog_button) {  view, _ ->
                            view.dismiss()
                        }
                        .setCancelable(false)
                        .create()
                dialog.show()
            }
        }

        whatsappButton.setOnClickListener{
            if(whatsappButton.isChecked) modulesList.add(ModuleNamesConstants.WHATSAPP_MODULE_NAME)
            else if (modulesList.contains(ModuleNamesConstants.WHATSAPP_MODULE_NAME)) modulesList.remove(ModuleNamesConstants.WHATSAPP_MODULE_NAME)
        }

        contactsButton.setOnClickListener{
            if(contactsButton.isChecked) modulesList.add(ModuleNamesConstants.CONTACTS_MODULE_NAME)
            else if (modulesList.contains(ModuleNamesConstants.CONTACTS_MODULE_NAME)) modulesList.remove(ModuleNamesConstants.CONTACTS_MODULE_NAME)
        }

        messagesButton.setOnClickListener {
            if(messagesButton.isChecked) modulesList.add(ModuleNamesConstants.MESSAGES_MODULE_NAME)
            else if (modulesList.contains(ModuleNamesConstants.MESSAGES_MODULE_NAME)) modulesList.remove(ModuleNamesConstants.MESSAGES_MODULE_NAME)
        }

        callsButton.setOnClickListener{
            if(callsButton.isChecked) modulesList.add(ModuleNamesConstants.CALLS_MODULE_NAME)
            else if (modulesList.contains(ModuleNamesConstants.CALLS_MODULE_NAME)) modulesList.remove(ModuleNamesConstants.CALLS_MODULE_NAME)
        }

        photosButton.setOnClickListener{
            if(photosButton.isChecked) modulesList.add(ModuleNamesConstants.PHOTOS_MODULE_NAME)
            else if (modulesList.contains(ModuleNamesConstants.PHOTOS_MODULE_NAME)) modulesList.remove(ModuleNamesConstants.PHOTOS_MODULE_NAME)
        }

        generalButton.setOnClickListener{
            if(generalButton.isChecked) modulesList.add(ModuleNamesConstants.GENERAL_INFO_MODULE_NAME)
            else if (modulesList.contains(ModuleNamesConstants.GENERAL_INFO_MODULE_NAME)) modulesList.remove(ModuleNamesConstants.GENERAL_INFO_MODULE_NAME)
        }    }

    private fun askPermissions(){
        if (ContextCompat.checkSelfPermission(this@MainActivity,
                        Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@MainActivity,
                    arrayOf(Manifest.permission.READ_CONTACTS), 1)
        }

        if (ContextCompat.checkSelfPermission(this@MainActivity,
                        Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@MainActivity,
                    arrayOf(Manifest.permission.READ_SMS), 2)
        }

        if (ContextCompat.checkSelfPermission(this@MainActivity,
                        Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@MainActivity,
                    arrayOf(Manifest.permission.READ_CALL_LOG), 3)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
}