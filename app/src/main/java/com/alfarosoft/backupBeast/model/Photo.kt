package com.alfarosoft.backupBeast.model

import android.net.Uri
import kotlinx.serialization.*

@Serializable
class Photo {
    var timestamp: String? = null
    var storageDir: String? = null
    var photoURI: String? = null

    constructor(timestamp: String?, storageDir: String?, photoURI: String?) {
        this.timestamp = timestamp
        this.storageDir = storageDir
        this.photoURI = photoURI
    }

    constructor()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Photo

        if (timestamp != other.timestamp) return false
        if (storageDir != other.storageDir) return false
        if (photoURI != other.photoURI) return false

        return true
    }

    override fun hashCode(): Int {
        var result = timestamp?.hashCode() ?: 0
        result = 31 * result + (storageDir?.hashCode() ?: 0)
        result = 31 * result + (photoURI?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Photo(timestamp=$timestamp, storageDir=$storageDir, photoURI=$photoURI)"
    }
}