package com.alfarosoft.backupBeast.model

import kotlinx.serialization.*

@Serializable
class Call {
    var callType: String? = null
    var date: String? = null
    var location: String? = null
    var number: String? = null
    var durationInSeconds: String? = null

    constructor()

    constructor(callType: String?, date: String?, location: String?, number: String?, durationInSeconds: String?) {
        this.callType = callType
        this.date = date
        this.location = location
        this.number = number
        this.durationInSeconds = durationInSeconds
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Call

        if (callType != other.callType) return false
        if (date != other.date) return false
        if (location != other.location) return false
        if (number != other.number) return false
        if (durationInSeconds != other.durationInSeconds) return false

        return true
    }

    override fun hashCode(): Int {
        var result = callType?.hashCode() ?: 0
        result = 31 * result + (date?.hashCode() ?: 0)
        result = 31 * result + (location?.hashCode() ?: 0)
        result = 31 * result + (number?.hashCode() ?: 0)
        result = 31 * result + (durationInSeconds?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Call(callType=$callType, date=$date, location=$location, number=$number, durationInSeconds=$durationInSeconds)"
    }

}