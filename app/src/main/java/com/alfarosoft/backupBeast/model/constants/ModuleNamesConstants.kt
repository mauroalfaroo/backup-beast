package com.alfarosoft.backupBeast.model.constants

object ModuleNamesConstants {
    const val GENERAL_INFO_MODULE_NAME = "General"
    const val WHATSAPP_MODULE_NAME = "WhatsApp"
    const val CONTACTS_MODULE_NAME = "Contacts"
    const val MESSAGES_MODULE_NAME = "Messages"
    const val CALLS_MODULE_NAME = "Calls"
    const val PHOTOS_MODULE_NAME = "Photos"
}