package com.alfarosoft.backupBeast.model.message
import kotlinx.serialization.*


@Serializable
class SMSMsg {
    var conversation_Id: String? = null
    var number: String? = null
    var type: String? = null
    var dateSent: String? = null
    var dateReceived: String? = null
    var subject: String? = null
    var text: String? = null

    constructor()

    constructor(conversation_Id: String?, number: String?, type: String?, dateSent: String?, dateReceived: String?, subject: String?, text: String?) {
        this.conversation_Id = conversation_Id
        this.number = number
        this.type = type
        this.dateSent = dateSent
        this.dateReceived = dateReceived
        this.subject = subject
        this.text = text
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SMSMsg

        if (conversation_Id != other.conversation_Id) return false
        if (number != other.number) return false
        if (type != other.type) return false
        if (dateSent != other.dateSent) return false
        if (dateReceived != other.dateReceived) return false
        if (subject != other.subject) return false
        if (text != other.text) return false

        return true
    }

    override fun hashCode(): Int {
        var result = conversation_Id?.hashCode() ?: 0
        result = 31 * result + (number?.hashCode() ?: 0)
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (dateSent?.hashCode() ?: 0)
        result = 31 * result + (dateReceived?.hashCode() ?: 0)
        result = 31 * result + (subject?.hashCode() ?: 0)
        result = 31 * result + (text?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Message(conversation_Id=$conversation_Id, number=$number, type=$type, dateSent=$dateSent, dateReceived=$dateReceived, subject=$subject, text=$text)"
    }


}