package com.alfarosoft.backupBeast.model.message
import kotlinx.serialization.*
import com.alfarosoft.backupBeast.model.Photo

@Serializable
class WhatsAppMessage {
    var conversation_Id: String? = null
    var fromMe: Boolean? = null
    var deleted: Boolean? = null
    var type: String? = null
    var date: String? = null
    var text: String? = null
    var photo: Photo? = null
    var handle: List<String> = ArrayList();
    var group: String? = null

    constructor()

    constructor(conversation_Id: String?, fromMe: Boolean?, deleted: Boolean?, type: String?, date: String?, text: String?, photo: Photo?, handle: List<String>, group: String?) {
        this.conversation_Id = conversation_Id
        this.fromMe = fromMe
        this.deleted = deleted
        this.type = type
        this.date = date
        this.text = text
        this.photo = photo
        this.handle = handle
        this.group = group
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WhatsAppMessage

        if (conversation_Id != other.conversation_Id) return false
        if (fromMe != other.fromMe) return false
        if (deleted != other.deleted) return false
        if (type != other.type) return false
        if (date != other.date) return false
        if (text != other.text) return false
        if (photo != other.photo) return false
        if (handle != other.handle) return false
        if (group != other.group) return false

        return true
    }

    override fun hashCode(): Int {
        var result = conversation_Id?.hashCode() ?: 0
        result = 31 * result + (fromMe?.hashCode() ?: 0)
        result = 31 * result + (deleted?.hashCode() ?: 0)
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (date?.hashCode() ?: 0)
        result = 31 * result + (text?.hashCode() ?: 0)
        result = 31 * result + (photo?.hashCode() ?: 0)
        result = 31 * result + handle.hashCode()
        result = 31 * result + (group?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "WhatsAppMessage(conversation_Id=$conversation_Id, fromMe=$fromMe, deleted=$deleted, type=$type, date=$date, text=$text, photo=$photo, handle=$handle, group=$group)"
    }

}