package com.alfarosoft.backupBeast.model
import kotlinx.serialization.*

@Serializable
class AppInfo {
    var appName: String? = null
    var packageName: String? = null
    var versionName: String? = null

    constructor()

    constructor(appName: String?, packageName: String?, versionName: String?) {
        this.appName = appName
        this.packageName = packageName
        this.versionName = versionName
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AppInfo

        if (appName != other.appName) return false
        if (packageName != other.packageName) return false
        if (versionName != other.versionName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = appName?.hashCode() ?: 0
        result = 31 * result + (packageName?.hashCode() ?: 0)
        result = 31 * result + (versionName?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "AppInfo(appName=$appName, packageName=$packageName, versionName=$versionName)"
    }


}