package com.alfarosoft.backupBeast.model
import kotlinx.serialization.*

@Serializable
class PhoneInfo {
    var manufacturer: String? = null
    var model: String? = null
    var device: String? = null
    var osVersion: String? = null
    var productName: String? = null
    var sdkVersion: String? = null
    var releaseVersion: String? = null
    var applicationsInstalled: List<AppInfo> = ArrayList();

    constructor()

    constructor(
            manufacturer: String?,
            model: String?,
            device: String?,
            osVersion: String?,
            productName: String?,
            sdkVersion: String?,
            releaseVersion: String?,
            applicationsInstalled: List<AppInfo>
    ) {
        this.manufacturer = manufacturer
        this.model = model
        this.device = device
        this.osVersion = osVersion
        this.productName = productName
        this.sdkVersion = sdkVersion
        this.releaseVersion = releaseVersion
        this.applicationsInstalled = applicationsInstalled
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PhoneInfo

        if (manufacturer != other.manufacturer) return false
        if (model != other.model) return false
        if (device != other.device) return false
        if (osVersion != other.osVersion) return false
        if (productName != other.productName) return false
        if (sdkVersion != other.sdkVersion) return false
        if (releaseVersion != other.releaseVersion) return false
        if (applicationsInstalled != other.applicationsInstalled) return false

        return true
    }

    override fun hashCode(): Int {
        var result = manufacturer?.hashCode() ?: 0
        result = 31 * result + (model?.hashCode() ?: 0)
        result = 31 * result + (device?.hashCode() ?: 0)
        result = 31 * result + (osVersion?.hashCode() ?: 0)
        result = 31 * result + (productName?.hashCode() ?: 0)
        result = 31 * result + (sdkVersion?.hashCode() ?: 0)
        result = 31 * result + (releaseVersion?.hashCode() ?: 0)
        result = 31 * result + applicationsInstalled.hashCode()
        return result
    }

    override fun toString(): String {
        return "PhoneInfo(manufacturer=$manufacturer, model=$model, device=$device, osVersion=$osVersion, productName=$productName, sdkVersion=$sdkVersion, releaseVersion=$releaseVersion, applicationsInstalled=$applicationsInstalled)"
    }


}