package com.alfarosoft.backupBeast.model

import kotlinx.serialization.*

@Serializable
class Contact {
    var id: String? = null
    var firstName: String? = null
    var middleName: String? = null
    var lastName: String? = null
    var email: String? = null
    var organization: String? = null
    var jobTitle: String? = null
    var department: String? = null
    var note: String? = null
    var phoneNumbers: Set<String> = emptySet()

    constructor(
        id: String?,
        firstName: String?,
        middleName: String?,
        lastName: String?,
        email: String?,
        organization: String?,
        jobTitle: String?,
        department: String?,
        note: String?,
        phoneNumbers: Set<String>
    ) {
        this.id = id
        this.firstName = firstName
        this.middleName = middleName
        this.lastName = lastName
        this.email = email
        this.organization = organization
        this.jobTitle = jobTitle
        this.department = department
        this.note = note
        this.phoneNumbers = phoneNumbers
    }

    constructor()


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Contact

        if (id != other.id) return false
        if (firstName != other.firstName) return false
        if (middleName != other.middleName) return false
        if (lastName != other.lastName) return false
        if (email != other.email) return false
        if (organization != other.organization) return false
        if (jobTitle != other.jobTitle) return false
        if (department != other.department) return false
        if (note != other.note) return false
        if (phoneNumbers != other.phoneNumbers) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (firstName?.hashCode() ?: 0)
        result = 31 * result + (middleName?.hashCode() ?: 0)
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (organization?.hashCode() ?: 0)
        result = 31 * result + (jobTitle?.hashCode() ?: 0)
        result = 31 * result + (department?.hashCode() ?: 0)
        result = 31 * result + (note?.hashCode() ?: 0)
        result = 31 * result + phoneNumbers.hashCode()
        return result
    }

    override fun toString(): String {
        return "Contact(id=$id, firstName=$firstName, middleName=$middleName, lastName=$lastName, email=$email, organization=$organization, jobTitle=$jobTitle, department=$department, note=$note, phoneNumbers=$phoneNumbers)"
    }

}