package com.alfarosoft.backupBeast.model.message

import android.graphics.Bitmap
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
class MMSMsg {
    var conversation_Id: String? = null
    var number: String? = null
    var type: String? = null
    var dateSent: String? = null
    var subject: String? = null
    var box: String? = null
    var body: String? = null
    @Contextual
    var mmsImage: Bitmap? = null

    constructor()

    constructor(conversation_Id: String?, number: String?, type: String?, dateSent: String?, subject: String?, box: String?, body: String?, mmsImage: Bitmap?) {
        this.conversation_Id = conversation_Id
        this.number = number
        this.type = type
        this.dateSent = dateSent
        this.subject = subject
        this.box = box
        this.body = body
        this.mmsImage = mmsImage
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MMSMsg

        if (conversation_Id != other.conversation_Id) return false
        if (number != other.number) return false
        if (type != other.type) return false
        if (dateSent != other.dateSent) return false
        if (subject != other.subject) return false
        if (box != other.box) return false
        if (body != other.body) return false
        if (mmsImage != other.mmsImage) return false

        return true
    }

    override fun hashCode(): Int {
        var result = conversation_Id?.hashCode() ?: 0
        result = 31 * result + (number?.hashCode() ?: 0)
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (dateSent?.hashCode() ?: 0)
        result = 31 * result + (subject?.hashCode() ?: 0)
        result = 31 * result + (box?.hashCode() ?: 0)
        result = 31 * result + (body?.hashCode() ?: 0)
        result = 31 * result + (mmsImage?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "MMSMsg(conversation_Id=$conversation_Id, number=$number, type=$type, dateSent=$dateSent, subject=$subject, box=$box, body=$body, mmsImage=$mmsImage)"
    }

}