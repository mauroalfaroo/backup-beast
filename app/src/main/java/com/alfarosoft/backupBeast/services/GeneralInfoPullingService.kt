package com.alfarosoft.backupBeast.services

import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import com.alfarosoft.backupBeast.model.AppInfo
import com.alfarosoft.backupBeast.model.PhoneInfo
import com.google.gson.Gson

class GeneralInfoPullingService(private val packageManager: PackageManager) {

    fun getPhoneGeneralInfo(): PhoneInfo? {
        val generalPhoneInfo = PhoneInfo()

        generalPhoneInfo.manufacturer = android.os.Build.BRAND
        generalPhoneInfo.model = android.os.Build.MODEL
        generalPhoneInfo.device = android.os.Build.DEVICE
        generalPhoneInfo.osVersion = android.os.Build.VERSION.BASE_OS
        generalPhoneInfo.productName = android.os.Build.PRODUCT
        generalPhoneInfo.sdkVersion = android.os.Build.VERSION.SDK_INT.toString()
        generalPhoneInfo.releaseVersion = android.os.Build.VERSION.RELEASE
        generalPhoneInfo.applicationsInstalled = getAppList()
        return generalPhoneInfo
    }

    private fun getAppList(): List<AppInfo> {
        val appList: MutableList<AppInfo> = ArrayList()
        val packageList: MutableList<PackageInfo> = packageManager.getInstalledPackages(0)

        for (p in packageList) {
            if (!isSystemPackage(p)) {
                val appInfo = AppInfo()
                appInfo.appName = p.applicationInfo.loadLabel(packageManager).toString()
                appInfo.packageName = p.applicationInfo.packageName
                appInfo.versionName = p.versionName
                appList.add(appInfo)
            }
        }

        return appList
    }

    private fun isSystemPackage(pkgInfo: PackageInfo): Boolean {
        return pkgInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM != 0
    }

    fun infoAsJson(phoneInfo: PhoneInfo?): String {
        val jsonString = Gson().toJson(phoneInfo)
        return jsonString
    }
}