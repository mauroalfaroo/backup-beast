package com.alfarosoft.backupBeast.services

import android.content.ContentResolver
import android.provider.CallLog
import com.alfarosoft.backupBeast.model.Call
import com.google.gson.Gson
import java.lang.Long
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CallLogPullingService (private val contentResolver: ContentResolver) {


    fun getCallLog(): List<Call>? {

        val callList: MutableList<Call> = ArrayList()
        val uri = CallLog.Calls.CONTENT_URI
        val sortOrder = CallLog.Calls.DEFAULT_SORT_ORDER

        contentResolver.query(uri, getCallLogProjection(), null, null, sortOrder)
                .use { cursor ->
                    if (cursor?.moveToFirst() == true) {
                        do {
                            val callToAdd = Call()
                            if (cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE)).equals("1")){
                                callToAdd.callType = "Incoming"
                            }
                            if (cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE)).equals("2")){
                                callToAdd.callType = "Outgoing"
                            }
                            if (cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE)).equals("3")){
                                callToAdd.callType = "Missed"
                            }

                            val callDate =  cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE))
                            val callDayTime = Date(Long.valueOf(callDate))
                            val formatter = SimpleDateFormat("dd-MM-yy HH:mm")
                            val dateString: String = formatter.format(callDayTime)
                            callToAdd.date = dateString
                            callToAdd.location =  cursor.getString(cursor.getColumnIndex(CallLog.Calls.GEOCODED_LOCATION))
                            callToAdd.number =  cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER))
                            callToAdd.durationInSeconds =  cursor.getString(
                                    cursor.getColumnIndex(
                                            CallLog.Calls.DURATION
                                    )
                            )
                            callList.add(callToAdd)
                        } while (cursor.moveToNext())
                    }
                }
        return callList
    }

    private fun getCallLogProjection() = arrayOf(
            CallLog.Calls.TYPE,
            CallLog.Calls.DATE,
            CallLog.Calls.GEOCODED_LOCATION,
            CallLog.Calls.NUMBER,
            CallLog.Calls.DURATION
    )

    fun listAsJson(callLog: List<Call>?): String {
        val jsonString = Gson().toJson(callLog)
        return jsonString
    }

}