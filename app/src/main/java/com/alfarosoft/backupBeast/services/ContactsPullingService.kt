package com.alfarosoft.backupBeast.services

import android.content.ContentResolver
import android.provider.ContactsContract
import com.alfarosoft.backupBeast.model.Contact
import com.google.gson.Gson

class ContactsPullingService(private val contentResolver: ContentResolver) {
    fun getContacts(): List<Contact>? {
        val contactList: MutableList<Contact> = ArrayList()
        val uri = ContactsContract.Data.CONTENT_URI
        val selection = "${ContactsContract.Data.MIMETYPE} = ? "
        val selectionArgs = arrayOf(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)

        contentResolver.query(uri, getContactProjection(), selection, selectionArgs, null)
                .use { cursor ->
                    if (cursor?.moveToFirst() == true) {
                        do {
                            val contactToAdd = Contact()
                            contactToAdd.id = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID))
                            contactToAdd.firstName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME))
                            contactToAdd.middleName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME))
                            contactToAdd.lastName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME))
                            contactToAdd.organization = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DISPLAY_NAME))
                            contactToAdd.department = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DEPARTMENT))
                            contactToAdd.jobTitle = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION))
                            contactToAdd.email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS))
                            contactToAdd.note = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE))
                            contactList.add(contactToAdd)
                        } while (cursor.moveToNext())
                    }
                }
        addPhoneNumbers(contactList)
        return contactList
    }

    private fun addPhoneNumbers(contacts: List<Contact>): List<Contact> {
        val phoneNumbers = mutableMapOf<Long, HashSet<String>>()
        val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val projection = getPhoneProjection()

        contentResolver
                .query(uri, projection, null, null, null)
                .use { cursor ->
                    if (cursor?.moveToFirst() == true) {
                        do {
                            val id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID))
                            val number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                                    ?: continue
                            if (phoneNumbers[id] == null) {
                                phoneNumbers[id] = HashSet()
                            }

                            phoneNumbers[id]?.add(number.replace(" ", ""))
                        } while (cursor.moveToNext())
                    }
                }

        phoneNumbers.forEach { (id, items) ->
            val contact: Contact? = contacts.find { it.id.equals(id.toString()) }
            if (contact != null) {
                contact.phoneNumbers = items.toSet()
            }
        }
        return contacts
    }

    private fun getContactProjection() = arrayOf(
            ContactsContract.Data.CONTACT_ID,
            ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
            ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME,
            ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
            ContactsContract.CommonDataKinds.Organization.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Organization.DEPARTMENT,
            ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION,
            ContactsContract.CommonDataKinds.Email.ADDRESS,
            ContactsContract.CommonDataKinds.Note.NOTE
    )

    private fun getPhoneProjection() = arrayOf(
            ContactsContract.Data.CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER
    )

    fun listAsJson(contacts: List<Contact>?): String {
        val jsonString = Gson().toJson(contacts)
        return jsonString
    }

}