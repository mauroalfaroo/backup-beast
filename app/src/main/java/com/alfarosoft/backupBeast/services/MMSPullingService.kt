package com.alfarosoft.backupBeast.services

import android.content.ContentResolver
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.Telephony
import com.alfarosoft.backupBeast.model.message.MMSMsg
import com.google.gson.Gson
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MMSPullingService(private val contentResolver: ContentResolver) {
    fun getMMSMessages(): List<MMSMsg>? {
        val MMSMsgList: MutableList<MMSMsg> = ArrayList()
        val uri = Telephony.Mms.CONTENT_URI
        val sortOrder = Telephony.Mms.DEFAULT_SORT_ORDER

        contentResolver.query(uri, getMMSProjection(), null, null, sortOrder)
                .use { cursor ->
                    if (cursor?.moveToFirst() == true) {
                        do {
                            val mmsToAdd = MMSMsg()
                            mmsToAdd.conversation_Id = cursor.getString(cursor.getColumnIndex(Telephony.Mms.THREAD_ID))
                            mmsToAdd.number = cursor.getString(cursor.getColumnIndex(Telephony.Mms.Addr.ADDRESS))
                            val type = cursor.getString(cursor.getColumnIndex(Telephony.Mms.Part.CT_TYPE))
                            val partId = cursor.getString(cursor.getColumnIndex(Telephony.Mms._ID))
                            if (type == "text/plain"){
                                mmsToAdd.type = "Plain text"
                            }
                            if ("image/jpeg" == type || "image/bmp" == type || "image/gif" == type || "image/jpg" == type || "image/png" == type) {
                                val bitmap: Bitmap? = getMmsImage(partId, contentResolver)
                                mmsToAdd.type = "Image"
                                mmsToAdd.mmsImage = bitmap
                            }

                            val dateSent = cursor.getString(cursor.getColumnIndex(Telephony.Mms.DATE))
                            val sentDayTime = Date(dateSent.toLong())
                            val formatter = SimpleDateFormat("dd-MM-yy HH:mm")
                            val dateSentString: String = formatter.format(sentDayTime)

                            mmsToAdd.dateSent = dateSentString
                            mmsToAdd.subject = cursor.getString(cursor.getColumnIndex(Telephony.Mms.SUBJECT))
                            mmsToAdd.box= cursor.getString(cursor.getColumnIndex(Telephony.Mms.MESSAGE_BOX))
                            mmsToAdd.body = getMmsText(partId, contentResolver)
                            MMSMsgList.add(mmsToAdd)
                        } while (cursor.moveToNext())
                    }
                }
        return MMSMsgList
    }

    private fun getMMSProjection() = arrayOf(
            Telephony.Mms._ID,
            Telephony.Mms.THREAD_ID,
            Telephony.Mms.Addr.ADDRESS,
            Telephony.Mms.Part.CT_TYPE,
            Telephony.Mms.DATE,
            Telephony.Mms.SUBJECT,
            Telephony.Mms.MESSAGE_BOX
    )

    private fun getMmsText(id: String, contentResolver: ContentResolver): String? {
        val partURI: Uri = Uri.parse("content://mms/part/$id")
        var inputStream: InputStream? = null
        val sb = StringBuilder()
        try {
            inputStream = contentResolver.openInputStream(partURI)
            if (inputStream != null) {
                val isr = InputStreamReader(inputStream, "UTF-8")
                val reader = BufferedReader(isr)
                var temp: String = reader.readLine()
                while (temp != null) {
                    sb.append(temp)
                    temp = reader.readLine()
                }
            }
        } catch (e: IOException) {
        } finally {
            if (inputStream!= null) {
                try {
                    inputStream.close()
                } catch (e: IOException) {
                }
            }
        }
        return sb.toString()
    }

    private fun getMmsImage(_id: String, contentResolver: ContentResolver): Bitmap? {
        val partURI = Uri.parse("content://mms/part/$_id")
        var inputStream: InputStream? = null
        var bitmap: Bitmap? = null
        try {
            inputStream = contentResolver.openInputStream(partURI)
            bitmap = BitmapFactory.decodeStream(inputStream)
        } catch (e: IOException) {
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (e: IOException) {
                }
            }
        }
        return bitmap
    }

    fun listAsJson(MMSMsgs: List<MMSMsg>?): String {
        val jsonString = Gson().toJson(MMSMsgs)
        return jsonString
    }
}