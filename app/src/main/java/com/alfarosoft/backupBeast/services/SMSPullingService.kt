package com.alfarosoft.backupBeast.services

import android.content.ContentResolver
import android.provider.Telephony
import com.alfarosoft.backupBeast.model.message.SMSMsg
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class SMSPullingService (private val contentResolver: ContentResolver) {

    fun getSMSMessages(): List<SMSMsg>? {
        val SMSMsgList: MutableList<SMSMsg> = ArrayList()
        val uri = Telephony.Sms.CONTENT_URI
        val sortOrder = Telephony.Sms.DEFAULT_SORT_ORDER

        contentResolver.query(uri, getSMSProjection(), null, null, sortOrder)
                .use { cursor ->
                    if (cursor?.moveToFirst() == true) {
                        do {
                            val smsToAdd = SMSMsg()
                            smsToAdd.conversation_Id = cursor.getString(cursor.getColumnIndex(Telephony.Sms.THREAD_ID))
                            smsToAdd.number = cursor.getString(cursor.getColumnIndex(Telephony.Sms.ADDRESS))

                            if (cursor.getString(cursor.getColumnIndex(Telephony.Sms.TYPE)).equals("1")){
                                smsToAdd.type = "Received"
                            }
                            if (cursor.getString(cursor.getColumnIndex(Telephony.Sms.TYPE)).equals("2")){
                                smsToAdd.type = "Sent"
                            }

                            val dateSent = cursor.getString(cursor.getColumnIndex(Telephony.Sms.DATE_SENT))
                            val dateReceived = cursor.getString(cursor.getColumnIndex(Telephony.Sms.DATE))
                            val sentDayTime = Date(dateSent.toLong())
                            val receivedDayTime = Date(dateReceived.toLong())
                            val formatter = SimpleDateFormat("dd-MM-yy HH:mm")
                            val dateSentString: String = formatter.format(sentDayTime)
                            val dateReceivedString: String = formatter.format(receivedDayTime)

                            smsToAdd.dateSent = dateSentString
                            smsToAdd.dateReceived = dateReceivedString
                            smsToAdd.subject = cursor.getString(cursor.getColumnIndex(Telephony.Sms.SUBJECT))
                            smsToAdd.text = cursor.getString(cursor.getColumnIndex(Telephony.Sms.BODY))
                            SMSMsgList.add(smsToAdd)
                        } while (cursor.moveToNext())
                    }
                }
        return SMSMsgList
    }

    private fun getSMSProjection() = arrayOf(
            Telephony.Sms.THREAD_ID,
            Telephony.Sms.ADDRESS,
            Telephony.Sms.TYPE,
            Telephony.Sms.DATE_SENT,
            Telephony.Sms.DATE,
            Telephony.Sms.SUBJECT,
            Telephony.Sms.BODY
    )

    fun listAsJson(SMSMsgs: List<SMSMsg>?): String {
        val jsonString = Gson().toJson(SMSMsgs)
        return jsonString
    }

}