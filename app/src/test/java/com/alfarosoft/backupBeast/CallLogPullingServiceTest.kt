package com.alfarosoft.backupBeast

import android.content.ContentResolver
import android.provider.CallLog
import android.provider.Telephony
import com.alfarosoft.backupBeast.model.Call
import com.alfarosoft.backupBeast.services.CallLogPullingService
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.same
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.fakes.RoboCursor

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class CallLogPullingServiceTest {
    lateinit var callLogCursor: RoboCursor
    lateinit var contentResolver: ContentResolver

    @Before
    fun setUp() {
        callLogCursor = RoboCursor()

        contentResolver = mock {
            on {
                query(
                        same(CallLog.Calls.CONTENT_URI),
                        anyOrNull(),
                        anyOrNull(),
                        anyOrNull(),
                        anyOrNull()
                )
            } doReturn callLogCursor
        }
        callLogCursor.setColumnNames(CALL_LOG_COLUMNS)
    }

    @Test
    fun testGetCallLog() {
        callLogCursor.setResults(
                arrayOf(
                        CALL_1,
                        CALL_2
                )
        )

        val callLogPullingService = CallLogPullingService(contentResolver)
        var callLogResult = callLogPullingService.getCallLog()

        if (callLogResult != null) {
            Assert.assertEquals(callLogResult.size, 2)
            Assert.assertEquals(callLogResult.get(0).callType, "Incoming")
            Assert.assertEquals(callLogResult.get(0).date, "20-07-21 19:19")
            Assert.assertEquals(callLogResult.get(0).location, "Omaha")
            Assert.assertEquals(callLogResult.get(0).number, "55567890")
            Assert.assertEquals(callLogResult.get(0).durationInSeconds, "256")
            Assert.assertEquals(callLogResult.get(1).callType, "Outgoing")
            Assert.assertEquals(callLogResult.get(1).date, "20-07-21 19:19")
            Assert.assertEquals(callLogResult.get(1).location, "Ciudad Autónoma de Buenos Aires")
            Assert.assertEquals(callLogResult.get(1).number, "55571234")
            Assert.assertEquals(callLogResult.get(1).durationInSeconds, "11")
        }
    }

    @Test
    fun testGetCallLogAsJson() {
        callLogCursor.setResults(
                arrayOf(
                        CallLogPullingServiceTest.CALL_1)
        )

        val callLogPullingService = CallLogPullingService(contentResolver)
        var callLogResult = callLogPullingService.getCallLog()
        var callLogAsJson = callLogPullingService.listAsJson(callLogResult)
        Assert.assertNotNull(callLogAsJson)

        //converting it to an object again for asserts
        val callLogFromJson = Gson().fromJson(callLogAsJson, Array<Call>::class.java)
        Assert.assertEquals(callLogFromJson.size, 1)
        Assert.assertEquals(callLogFromJson.get(0).callType, "Incoming")
        Assert.assertEquals(callLogFromJson.get(0).date, "20-07-21 19:19")
        Assert.assertEquals(callLogFromJson.get(0).location, "Omaha")
        Assert.assertEquals(callLogFromJson.get(0).number, "55567890")
        Assert.assertEquals(callLogFromJson.get(0).durationInSeconds, "256")
    }

    companion object MockData {
        private val CALL_LOG_COLUMNS = listOf(
                CallLog.Calls.TYPE,
                CallLog.Calls.DATE,
                CallLog.Calls.GEOCODED_LOCATION,
                CallLog.Calls.NUMBER,
                CallLog.Calls.DURATION
        )

        private val CALL_1 = arrayOf(
                "1",
                "1626819574090",
                "Omaha",
                "55567890",
                "256"
        )

        private val CALL_2 = arrayOf(
                "2",
                "1626819574090",
                "Ciudad Autónoma de Buenos Aires",
                "55571234",
                "11"
        )
    }
}