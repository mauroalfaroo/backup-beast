package com.alfarosoft.backupBeast

import android.content.Context
import android.content.pm.PackageManager
import com.alfarosoft.backupBeast.services.GeneralInfoPullingService
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class GeneralInfoPullingServiceTest {
    lateinit var packageManager: PackageManager
    lateinit var mContext : Context
    lateinit var generalInfoPullingService: GeneralInfoPullingService

    @Before
    fun setUp() {
        packageManager = Mockito.mock(PackageManager::class.java)
    }

    @Test
    fun testGetGeneralPhoneInfo() {
        val realService = GeneralInfoPullingService(packageManager)

        //this returns the info from the Robolectric mocked device
        val actualInfo = realService.getPhoneGeneralInfo()

        if (actualInfo != null) {
            Assert.assertEquals(actualInfo.manufacturer, "Android")
            Assert.assertEquals(actualInfo.model, "robolectric")
            Assert.assertEquals(actualInfo.device, "robolectric")
            Assert.assertEquals(actualInfo.osVersion, "")
            Assert.assertEquals(actualInfo.productName, "robolectric")
            Assert.assertEquals(actualInfo.sdkVersion, "28")
            Assert.assertEquals(actualInfo.releaseVersion, "9")
        }
    }


}