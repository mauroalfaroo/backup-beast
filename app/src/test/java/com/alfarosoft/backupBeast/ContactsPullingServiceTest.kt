package com.alfarosoft.backupBeast

import android.content.ContentResolver
import android.provider.ContactsContract
import com.alfarosoft.backupBeast.model.Contact
import com.alfarosoft.backupBeast.services.ContactsPullingService
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.same
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.fakes.RoboCursor

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class ContactsPullingServiceTest {
    lateinit var contactsCursor: RoboCursor
    lateinit var phoneNumbersCursor: RoboCursor
    lateinit var contentResolver: ContentResolver

    @Before
    fun setUp() {
        contactsCursor = RoboCursor()
        phoneNumbersCursor = RoboCursor()

        contentResolver = mock {
            on {
                query(
                        same(ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
                        anyOrNull(),
                        anyOrNull(),
                        anyOrNull(),
                        anyOrNull()
                )
            } doReturn phoneNumbersCursor
            on {
                query(
                        same(ContactsContract.Data.CONTENT_URI),
                        anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull()
                )
            } doReturn contactsCursor
        }
        contactsCursor.setColumnNames(CONTACTS_COLUMNS)
        phoneNumbersCursor.setColumnNames(PHONES_COLUMNS)
    }

    @Test
    fun testGetAllContacts() {
        contactsCursor.setResults(
                arrayOf(
                        CONTACT_1_FULL,
                        CONTACT_2_FULL,
                        CONTACT_3_EMPTY
                )
        )
        phoneNumbersCursor.setResults(
                arrayOf(
                        PHONE_1_CONTACT_1,
                        PHONE_2_CONTACT_1,
                        PHONE_2_CONTACT_2_OK,
                        PHONE_1_CONTACT_2_EMPTY
                )
        )

        val contactsPullingService = ContactsPullingService(contentResolver)
        var contactsResult = contactsPullingService.getContacts()
        if (contactsResult != null) {
            assertEquals(contactsResult.size, 3)
            assertEquals(contactsResult.get(0).firstName, "given name")
            assertEquals(contactsResult.get(0).middleName, "middle name")
            assertEquals(contactsResult.get(0).lastName, "family name")
            assertEquals(contactsResult.get(0).jobTitle, "jobTitle")
            assertEquals(contactsResult.get(1).firstName, "given name2")
            assertEquals(contactsResult.get(1).middleName, "middle name2")
            assertEquals(contactsResult.get(1).lastName, "family name2")
            assertEquals(contactsResult.get(0).phoneNumbers.size, 2)
            assertEquals(contactsResult.get(1).phoneNumbers.size, 1)
            assertEquals(contactsResult.get(2).phoneNumbers.size, 0)
        }
    }

    @Test
    fun testContactsAsJson() {
        contactsCursor.setResults(
                arrayOf(
                        CONTACT_1_FULL,
                        CONTACT_2_FULL,
                        CONTACT_3_EMPTY
                )
        )
        phoneNumbersCursor.setResults(
                arrayOf(
                        PHONE_1_CONTACT_1,
                        PHONE_2_CONTACT_1,
                        PHONE_2_CONTACT_2_OK,
                        PHONE_1_CONTACT_2_EMPTY
                )
        )
        val contactsPullingService = ContactsPullingService(contentResolver)
        var contactsResult = contactsPullingService.getContacts()
        var contactsAsJson = contactsPullingService.listAsJson(contactsResult)
        assertNotNull(contactsAsJson)

        //converting it to an object again for asserts
        val contactFromJSon = Gson().fromJson(contactsAsJson, Array<Contact>::class.java)
        assertEquals(contactFromJSon.get(0).id, "1")
        assertEquals(contactFromJSon.get(0).firstName, "given name")
        assertEquals(contactFromJSon.get(0).middleName, "middle name")
        assertEquals(contactFromJSon.get(0).lastName, "family name")
        assertEquals(contactFromJSon.get(0).jobTitle, "jobTitle")
        assertEquals(contactFromJSon.get(1).firstName, "given name2")
        assertEquals(contactFromJSon.get(1).middleName, "middle name2")
        assertEquals(contactFromJSon.get(1).lastName, "family name2")
        assertEquals(contactFromJSon.get(0).phoneNumbers.size, 2)
        assertEquals(contactFromJSon.get(1).phoneNumbers.size, 1)
        assertEquals(contactFromJSon.get(2).phoneNumbers.size, 0)
    }

    companion object MockData {
        private val CONTACTS_COLUMNS = listOf(
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME,
                ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
                ContactsContract.CommonDataKinds.Organization.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Organization.DEPARTMENT,
                ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION,
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Note.NOTE
        )

        private val PHONES_COLUMNS = listOf(
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        )

        private val CONTACT_1_ID = 1L
        private val CONTACT_2_ID = 2L
        private val CONTACT_3_ID = 3L

        private val CONTACT_1_FULL = arrayOf(
                CONTACT_1_ID,
                "given name",
                "middle name",
                "family name",
                "email address",
                "organization",
                "jobTitle",
                "jobDepartment",
                "note"
        )

        private val CONTACT_2_FULL = arrayOf(
                CONTACT_2_ID,
                "given name2",
                "middle name2",
                "family name2",
                "email address",
                "organization",
                "jobTitle",
                "jobDepartment",
                "note"
        )

        private val CONTACT_3_EMPTY = arrayOf(
                CONTACT_3_ID,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        )

        private val PHONE_1_CONTACT_1 = arrayOf(
                CONTACT_1_ID,
                "1113343431"
        )

        private val PHONE_2_CONTACT_1 = arrayOf(
                CONTACT_1_ID,
                "9999999999"
        )

        private val PHONE_1_CONTACT_2_EMPTY = arrayOf(
                CONTACT_2_ID,
                null
        )

        private val PHONE_2_CONTACT_2_OK = arrayOf(
                CONTACT_2_ID,
                "5555555555"
        )
    }
}