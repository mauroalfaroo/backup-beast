package com.alfarosoft.backupBeast

import android.content.ContentResolver
import android.provider.Telephony
import com.alfarosoft.backupBeast.model.message.SMSMsg
import com.alfarosoft.backupBeast.services.SMSPullingService
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.same
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.fakes.RoboCursor

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class SMSPullingServiceTest {
    lateinit var smsCursor: RoboCursor
    lateinit var contentResolver: ContentResolver

    @Before
    fun setUp() {
        smsCursor = RoboCursor()

        contentResolver = mock {
            on {
                query(
                        same(Telephony.Sms.CONTENT_URI),
                        anyOrNull(),
                        anyOrNull(),
                        anyOrNull(),
                        anyOrNull()
                )
            } doReturn smsCursor
        }
        smsCursor.setColumnNames(SMS_COLUMNS)
    }

    @Test
    fun testGetAllMessages() {
        smsCursor.setResults(
                arrayOf(
                        SMS_1
                )
        )

        val smsPullingService = SMSPullingService(contentResolver)
        var smsResult = smsPullingService.getSMSMessages()

        if (smsResult != null) {
            Assert.assertEquals(smsResult.size, 1)
            Assert.assertEquals(smsResult.get(0).conversation_Id, "1")
            Assert.assertEquals(smsResult.get(0).number, "111222333")
            Assert.assertEquals(smsResult.get(0).type, "Received")
            Assert.assertEquals(smsResult.get(0).dateSent, "20-07-21 19:19")
            Assert.assertEquals(smsResult.get(0).dateReceived, "20-07-21 19:19")
            Assert.assertEquals(smsResult.get(0).subject, "A subject")
            Assert.assertEquals(smsResult.get(0).text, "this is the text message")

        }
    }

    @Test
    fun testgetMessagesAsJson() {
        smsCursor.setResults(
                arrayOf(
                        SMS_1
                )
        )

        val smsPullingService = SMSPullingService(contentResolver)
        var smsResult = smsPullingService.getSMSMessages()
        var smsAsJson = smsPullingService.listAsJson(smsResult)
        Assert.assertNotNull(smsAsJson)

        //converting it to an object again for asserts
        val smsFromJson = Gson().fromJson(smsAsJson, Array<SMSMsg>::class.java)
        Assert.assertEquals(smsFromJson.size, 1)
        Assert.assertEquals(smsFromJson.get(0).conversation_Id, "1")
        Assert.assertEquals(smsFromJson.get(0).number, "111222333")
        Assert.assertEquals(smsFromJson.get(0).type, "Received")
        Assert.assertEquals(smsFromJson.get(0).dateSent, "20-07-21 19:19")
        Assert.assertEquals(smsFromJson.get(0).dateReceived, "20-07-21 19:19")
        Assert.assertEquals(smsFromJson.get(0).subject, "A subject")
        Assert.assertEquals(smsFromJson.get(0).text, "this is the text message")
    }

    companion object MockData {
        private val SMS_COLUMNS = listOf(
                Telephony.Sms.THREAD_ID,
                Telephony.Sms.ADDRESS,
                Telephony.Sms.TYPE,
                Telephony.Sms.DATE_SENT,
                Telephony.Sms.DATE,
                Telephony.Sms.SUBJECT,
                Telephony.Sms.BODY
        )

        private val SMS_1 = arrayOf(
                "1",
                "111222333",
                "1",
                "1626819574090",
                "1626819574090",
                "A subject",
                "this is the text message"
        )

    }

}
