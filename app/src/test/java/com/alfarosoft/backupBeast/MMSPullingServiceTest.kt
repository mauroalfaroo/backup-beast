package com.alfarosoft.backupBeast

import android.content.ContentResolver
import android.provider.Telephony
import com.alfarosoft.backupBeast.model.message.MMSMsg
import com.alfarosoft.backupBeast.services.MMSPullingService
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.same
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.fakes.RoboCursor

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class MMSPullingServiceTest {
    lateinit var mmsCursor: RoboCursor
    lateinit var contentResolver: ContentResolver

    @Before
    fun setUp() {
        mmsCursor = RoboCursor()

        contentResolver = mock {
            on {
                query(
                        same(Telephony.Mms.CONTENT_URI),
                        anyOrNull(),
                        anyOrNull(),
                        anyOrNull(),
                        anyOrNull()
                )
            } doReturn mmsCursor
        }
        mmsCursor.setColumnNames(MMSPullingServiceTest.MMS_COLUMNS)
    }

    @Test
    fun testGetAllMMS() {
        mmsCursor.setResults(
                arrayOf(
                        MMS_1
                )
        )

        val mmsPullingService = MMSPullingService(contentResolver)
        var mmsResult = mmsPullingService.getMMSMessages()

        if (mmsResult != null) {
            Assert.assertEquals(mmsResult.size, 1)
            Assert.assertEquals(mmsResult.get(0).conversation_Id, "123")
            Assert.assertEquals(mmsResult.get(0).number, "551445678")
            Assert.assertEquals(mmsResult.get(0).type, "Plain text")
            Assert.assertEquals(mmsResult.get(0).dateSent, "20-07-21 19:19")
            Assert.assertEquals(mmsResult.get(0).subject, "A subject")
            Assert.assertEquals(mmsResult.get(0).box, "Inbox")
        }
    }

    @Test
    fun testgetMMSAsJson() {
        mmsCursor.setResults(
                arrayOf(
                        MMS_1
                )
        )

        val mmsPullingService = MMSPullingService(contentResolver)
        var mmsResult = mmsPullingService.getMMSMessages()
        var mmsAsJson = mmsPullingService.listAsJson(mmsResult)
        Assert.assertNotNull(mmsAsJson)

        //converting it to an object again for asserts
        val mmssFromJson = Gson().fromJson(mmsAsJson, Array<MMSMsg>::class.java)
        Assert.assertEquals(mmssFromJson.size, 1)
        Assert.assertEquals(mmssFromJson.get(0).conversation_Id, "123")
        Assert.assertEquals(mmssFromJson.get(0).number, "551445678")
        Assert.assertEquals(mmssFromJson.get(0).type, "Plain text")
        Assert.assertEquals(mmssFromJson.get(0).dateSent, "20-07-21 19:19")
        Assert.assertEquals(mmssFromJson.get(0).subject, "A subject")
        Assert.assertEquals(mmssFromJson.get(0).box, "Inbox")
    }


    companion object MockData {
        private val MMS_COLUMNS = listOf(
                Telephony.Mms._ID,
                Telephony.Mms.THREAD_ID,
                Telephony.Mms.Addr.ADDRESS,
                Telephony.Mms.Part.CT_TYPE,
                Telephony.Mms.DATE,
                Telephony.Mms.SUBJECT,
                Telephony.Mms.MESSAGE_BOX
        )

        private val MMS_1 = arrayOf(
                "1",
                "123",
                "551445678",
                "text/plain",
                "1626819574090",
                "A subject",
                "Inbox"
        )

    }
}