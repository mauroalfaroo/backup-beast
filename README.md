# backup-beast
Android app made with Kotlin. Exports contacts, SMS, phone calls, photos and general phone info to a JSON file.

- Need to explicitly give permissions to the app to export the data.
- Upcoming feature: The app allows the user to select to where export the JSON files (Google Drive, AWS S3 bucket, device storage, etc).
- Upcoming feature: Two-factor authentication (probably via SMS or e-mail).

## Update July 2021 - First Alpha release

[![GitHub release (latest by date)](https://img.shields.io/github/v/release/mauroalfaro/backup-beast?include_prereleases)](https://github.com/mauroalfaro/backup-beast/releases/tag/v0.1-alpha)

Kind of a developer-oriented release, since the info is logged on Logcat and is still not exported to a physical JSON file. Looking forward to include that on alpha 2 or maybe 3.

Working modules for this version:
- Contacts
- Messages (SMS and MMS)
- Calls

Included APK to test on physical devices. The best way to test it and see logs is cloning the repository and build it via Android Studio.
